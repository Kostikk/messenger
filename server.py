from flask import Flask, request, abort, jsonify
import datetime, time
app = Flask(__name__)

messages = [
    {"username": "Pisos", "text": "ti pidor", "time": time.time()},
    {"username": "Sanya", "text": "no u", "time": time.time()},

]

users = {
    'Pisos':'256936',
    'Sanya':'256936'
}

count_messeges = len(messages)
count_users = len(users)

@app.route("/")
def return_hello():
    return "Hello, World!"

@app.route("/messages")
def return_messages():
    m = messages
    for message in m:
        if type(message["time"]) == str:
            pass
        else:
            correct_time = datetime.datetime.fromtimestamp(message["time"])
            message["time"] = correct_time.strftime('%d-%m-%Y %H:%M:%S')
    return {"messages": m}

@app.route("/status")
def return_status_and_time():
    time = datetime.datetime.now()
    status = {'status':'ok', 'time': time.strftime("%d-%m-%Ys %H:%M:%S")}
    return ('status - ' + status['status'] + ' ' +'time - '+ status['time']
    + ' ' + 'users - ' + str(count_users) + ' ' + 'messeges - '  + str(count_messeges))


@app.errorhandler(404)
def resource_not_found(e):
    return abort(404, description="Resource not found")

# @app.errorhandler(403)
# def resource_not_found(e):
#     return abort(403, description="Auth required")


@app.route("/auth", methods=['POST'])
def auth():
    data = request.json
    username = data["username"]
    password = data["password"]
    if username not in users:
        users[username] = password
        response_data = jsonify({'code': 200, "ok": True})
        response_data.status_code = 200
        return response_data
    elif users[username] == password:
        response_data = jsonify({'code': 200, "ok": True})
        response_data.status_code = 200
        return response_data
    else:
        response_data = jsonify({'code': 401,'message': 'Wrong password'})
        response_data.status_code = 401
        return response_data


@app.route("/send", methods=['POST'])
def send():
    data = request.json
    username = data["username"]
    password = data["password"]


    if username not in users or users[username] != password:
        return {'ok':False}

    text = data["text"]
    messages.append({"username": username, "text": text, "time": time.time()})
    return 'okay'
app.run(host='192.168.42.197')